Ansible playbook to setup HTTPS using Let's encrypt on nginx.

The Ansible playbook installs everything needed to serve static files from a nginx server over HTTPS.
The server pass A rating on [SSL Labs](https://www.ssllabs.com/).

To use:
 1. Install [Ansible](https://www.ansible.com/)
 2. Setup an Ubuntu 16.04 server accessible over ssh, http, https. Assign DNS name label to the server.
 3. Update `hosts`and change ip address and DNS name label to your domain
 4. Run `ansible-playbook -i hosts playbook.yml`
 5. Login to VM
 6. Edit /etc/nginx/sites-available/default for server port 80

        location /.well-known {
                alias /var/www/html/.well-known;
        }
 7. Reload nginx 'service nginx reload'
 8. Execute certbot to generat certs only

certbot certonly -n --webroot -w /var/www/html -m rmontag@airplus.com --agree-tos -d ex532vma.germanycentral.cloudapp.microsoftazure.de --staging

 9. Check result
10. Edit /etc/nginx/sites-available/default for server port 443

server {
        listen 443 ssl;
        server_name ex532vma.germanycentral.cloudapp.microsoftazure.de;
        ssl_certificate /etc/letsencrypt/live/ex532vma.germanycentral.cloudapp.microsoftazure.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ex532vma.germanycentral.cloudapp.microsoftazure.de/privkey.pem;
}

11. Reload nginx 'service nginx reload'


-------


Daily cronjob, will only renew, if certs expire within 30 days:
0 6 * * * /path/to/certbot/certbot-auto renew --text >> /path/to/certbot/certbot-cron.log && sudo service nginx reload

 
