i(1) ResourceGroup anlegen: az group create --name ex532 --location germanycentral

(2) VirtualNetwork anlegen: az network create --resource-group ex532ResourceGroup --name ex532Vnet --address-prefix 10.2.0.0/16 --subnet-name ex532Subnet --subnet-prefix 12.2.1.0/24

(3) Create file azure_create_complete_vm.yml

(4) Execute playbook
$ ansible-playbook azure_create_complete_vm.yml

